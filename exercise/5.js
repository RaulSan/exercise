//4.- Fizz buzz, every number that can be divided by 3 shows fizz, by 5 shows buzz
//by 3 and by 5 shows fizzbuzz, if 0 or 1 show fizz
//Code here

module.exports = fizzBuzz;

// Example
// 1 fizz
// 3 fizz
// 5 buzz
// 15 fizzbuzz
