//3.-Given a string return  true if length equal to 4 digits
// with a function call isThreeDigits
//Code here

module.exports = isThreeDigits;

//Example

// treeDigits(20) expected false
// treeDigits(8400) expected true

//10 would be false, 1000 would be true
// 25 would false
// 4500 would be true
