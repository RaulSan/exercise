//4.- Make a function call invertStr to invert a string
//Code here

module.exports = invertStr;
// Example

// invertString('Hello') expected 'olleH'

// Hello would be olleH
// Jose would be esoJ
