const fizzBuzz = require("../exercise/5");

test("Return 3, fizz, 5, buzz, 3 and  5 fizzbuzz", () => {
  expect(fizzBuzz(0)).toBe("fizz");
  expect(fizzBuzz(1)).toBe("fizz");
  expect(fizzBuzz(2)).toBe("fizz");
  expect(fizzBuzz(3)).toBe("fizz");
  expect(fizzBuzz(5)).toBe("buzz");
  expect(fizzBuzz(15)).toBe("fizzbuzz");
});
