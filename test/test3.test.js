const isThreeDigits = require("../exercise/3");

test("The string length is equal or bigger than 4", () => {
  expect(isThreeDigits("Jose")).toBeTruthy();
});
