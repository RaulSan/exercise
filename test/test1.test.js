const sum = require( "../exercise/1" );

test("add 5 + 10 = 15", () => {
  expect(sum(5, 10)).toBe(15);
});
