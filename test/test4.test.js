const invertStr = require("../exercise/4");

test("Return str invert", () => {
  expect(invertStr("hello")).toBe("olleh");
});
