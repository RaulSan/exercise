const bigger = require("../exercise/2");

test("Return bigger number", () => {
  expect(bigger(8, 2)).toBe(8);
  expect(bigger(8, 8)).toBe(0);
});
